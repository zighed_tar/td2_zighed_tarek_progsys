#include <iostream>
#include <sstream>
#include <random>
#include <thread>
#include <mutex>
#include "classes.hpp"
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

using namespace boost::interprocess;

int main() {
    shared_memory_object shm_obj( open_only, "shared_memory", read_write );
    mapped_region region(shm_obj, read_write);
    void * addr = region.get_address();
    MessageBox * box = static_cast<MessageBox *>(addr);
    const unsigned nb_messages = 20;
    const unsigned nb_consumers = 2;
    Random random( 50 );
    std::cout << "start" << std::endl;
    std::vector< std::thread > group;
    for( unsigned i = 0; i < nb_consumers; ++i ) {
        group.push_back( std::thread( Consumer( i + 1, *box, random, nb_messages )));
    }
    for( auto & th : group ) {
        th.join();
    }
    std::cout << "finish" << std::endl;
    shared_memory_object::remove("shared_memory");
    return 0;
}
