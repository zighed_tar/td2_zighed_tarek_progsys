#include <iostream>
#include <sstream>
#include <random>
#include <thread>
#include <mutex>
#include "classes.hpp"
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

using namespace boost::interprocess;

int main() {
    shared_memory_object shm_obj(
     open_or_create, "shared_memory", read_write );
    shm_obj.truncate(sizeof(MessageBox));
    mapped_region region(shm_obj, read_write);
    void * addr = region.get_address();
    MessageBox * box = new ( addr ) MessageBox();
    const unsigned nb_messages = 10;
    const unsigned nb_producers = 4;
    Random random( 50 );
    std::cout << "start" << std::endl;
    std::vector< std::thread > group;
    for( unsigned i = 0; i < nb_producers; ++i ) {
        group.push_back( std::thread( Producer( i + 1, *box, random, nb_messages )));
    }
    for( auto & th : group ) {
        th.join();
    }
    std::cout << "finish" << std::endl;
    return 0;
}
